function openCity(evt, cityName) {
    var i, x, tablinks;
    x = document.getElementsByClassName("tab");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < x.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" tabs-border-red", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.firstElementChild.className += " tabs-border-red";
}


jQuery(document).ready(function () {
    jQuery('.edit-action').click(function () {
        jQuery('tr[data-id="' + jQuery(this).attr('data-id') + '"] td').each(function () {
            jQuery(this).find('.row-data').hide()
            jQuery(this).find('.row-data-input').show()
        })

        jQuery(this).css('display', 'none');
        jQuery('.update-action[data-id="' + jQuery(this).attr('data-id') + '"]').css('display', 'inline-block');
        jQuery('.cancel-action[data-id="' + jQuery(this).attr('data-id') + '"]').css('display', 'inline-block');

    })

    jQuery('.cancel-action').click(function () {
        let rowID = jQuery(this).attr('data-id');
        jQuery(this).css('display', 'none');
        jQuery('.update-action[data-id="' + rowID + '"]').css('display', 'none');
        jQuery('.edit-action[data-id="' + rowID + '"]').css('display', 'inline-block');
        jQuery('tr[data-id="' + rowID + '"] .row-data-input').css('display', 'none');
        jQuery('tr[data-id="' + rowID + '"] .row-data').css('display', 'block');
    })

    jQuery(".update-action").click(function (e) {
        e.preventDefault();

        // collect updated values
        let rowID = jQuery(this).attr('data-id'),
            title = jQuery('tr[data-id="' + rowID + '"] input[name="row_title"]').val(),
            content = jQuery('tr[data-id="' + rowID + '"] textarea[name="row_content"]').val(),
            category = jQuery('tr[data-id="' + rowID + '"] select[name="category_item"] option:selected').val(),
            categoryName = jQuery('tr[data-id="' + rowID + '"] select[name="category_item"] option:selected').text();

        jQuery.ajax({
            type: "post",
            dataType: "json",
            url: myAjax.ajaxurl,
            data: {
                action: "my_user_vote",
                post_id: rowID,
                title: title,
                content: content,
                category: category,
            },
            success: function (response) {
                if (response.type == "success") {
                    // buttons
                    jQuery('.update-action[data-id="' + rowID + '"]').css('display', 'none');
                    jQuery('.edit-action[data-id="' + rowID + '"]').css('display', 'inline-block');
                    jQuery('.cancel-action[data-id="' + rowID + '"]').css('display', 'none');

                    // fields
                    jQuery('tr[data-id="' + rowID + '"] .row-data-input').css('display', 'none');
                    jQuery('tr[data-id="' + rowID + '"] .row-data').css('display', 'block');

                    // update values
                    jQuery('#row_title_' + rowID).text(title)
                    jQuery('#row_content_' + rowID).text(content)
                    jQuery('#category_item_' + rowID).text(categoryName)
                    jQuery('#report_title_' + rowID).text(title)
                    jQuery('#report_content_' + rowID).text(content)
                    jQuery('#report_category_' + rowID).text(categoryName)
                } else {
                    alert("Your request could not be done")
                }
            }
        })

    })

    jQuery(".delete-action").click(function (e) {
        e.preventDefault();

        // collect updated values
        let rowID = jQuery(this).attr('data-id');

        jQuery.ajax({
            type: "post",
            dataType: "json",
            url: myAjax.ajaxurl,
            data: {
                action: "delete_row",
                post_id: rowID,
            },
            success: function (response) {
                console.log(response.type)
                if (response.type == "success") {
                    console.log('tr[data-id="' + rowID + '"]')
                    jQuery('tr[data-id="' + rowID + '"]').remove()
                } else {
                    alert("Your request could not be done")
                }
            }
        })

    })

})