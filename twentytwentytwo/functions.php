<?php
/**
 * Twenty Twenty-Two functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_Two
 * @since Twenty Twenty-Two 1.0
 */


if (!function_exists('twentytwentytwo_support')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * @return void
     * @since Twenty Twenty-Two 1.0
     *
     */
    function twentytwentytwo_support()
    {

        // Add support for block styles.
        add_theme_support('wp-block-styles');

        // Enqueue editor styles.
        add_editor_style('style.css');

    }

endif;

add_action('after_setup_theme', 'twentytwentytwo_support');

if (!function_exists('twentytwentytwo_styles')) :

    /**
     * Enqueue styles.
     *
     * @return void
     * @since Twenty Twenty-Two 1.0
     *
     */
    function twentytwentytwo_styles()
    {
        // Register theme stylesheet.
        $theme_version = wp_get_theme()->get('Version');

        $version_string = is_string($theme_version) ? $theme_version : false;
        wp_register_style(
            'twentytwentytwo-style',
            get_template_directory_uri() . '/style.css',
            array(),
            $version_string
        );

        // Add styles inline.
        wp_add_inline_style('twentytwentytwo-style', twentytwentytwo_get_font_face_styles());

        // Enqueue theme stylesheet.
        wp_enqueue_style('twentytwentytwo-style');

    }

endif;

add_action('wp_enqueue_scripts', 'twentytwentytwo_styles');

if (!function_exists('twentytwentytwo_editor_styles')) :

    /**
     * Enqueue editor styles.
     *
     * @return void
     * @since Twenty Twenty-Two 1.0
     *
     */
    function twentytwentytwo_editor_styles()
    {

        // Add styles inline.
        wp_add_inline_style('wp-block-library', twentytwentytwo_get_font_face_styles());

    }

endif;

add_action('admin_init', 'twentytwentytwo_editor_styles');


if (!function_exists('twentytwentytwo_get_font_face_styles')) :

    /**
     * Get font face styles.
     * Called by functions twentytwentytwo_styles() and twentytwentytwo_editor_styles() above.
     *
     * @return string
     * @since Twenty Twenty-Two 1.0
     *
     */
    function twentytwentytwo_get_font_face_styles()
    {

        return "
		@font-face{
			font-family: 'Source Serif Pro';
			font-weight: 200 900;
			font-style: normal;
			font-stretch: normal;
			font-display: swap;
			src: url('" . get_theme_file_uri('assets/fonts/SourceSerif4Variable-Roman.ttf.woff2') . "') format('woff2');
		}

		@font-face{
			font-family: 'Source Serif Pro';
			font-weight: 200 900;
			font-style: italic;
			font-stretch: normal;
			font-display: swap;
			src: url('" . get_theme_file_uri('assets/fonts/SourceSerif4Variable-Italic.ttf.woff2') . "') format('woff2');
		}
		";

    }

endif;

if (!function_exists('twentytwentytwo_preload_webfonts')) :

    /**
     * Preloads the main web font to improve performance.
     *
     * Only the main web font (font-style: normal) is preloaded here since that font is always relevant (it is used
     * on every heading, for example). The other font is only needed if there is any applicable content in italic style,
     * and therefore preloading it would in most cases regress performance when that font would otherwise not be loaded
     * at all.
     *
     * @return void
     * @since Twenty Twenty-Two 1.0
     *
     */
    function twentytwentytwo_preload_webfonts()
    {
        ?>
        <link rel="preload"
              href="<?php echo esc_url(get_theme_file_uri('assets/fonts/SourceSerif4Variable-Roman.ttf.woff2')); ?>"
              as="font" type="font/woff2" crossorigin>
        <?php
    }

endif;

add_action('wp_head', 'twentytwentytwo_preload_webfonts');

// Add block patterns
require get_template_directory() . '/inc/block-patterns.php';

function wpb_adding_scripts()
{

    wp_register_script('my_js', get_theme_file_uri('assets/script.js', __FILE__), array('jquery'), time(), true);
    wp_localize_script('my_js', 'myAjax', array('ajaxurl' => admin_url('admin-ajax.php')));
    wp_enqueue_script('my_js');
}

add_action('wp_enqueue_scripts', 'wpb_adding_scripts');


add_action("wp_ajax_my_user_vote", "my_user_vote");
add_action("wp_ajax_nopriv_my_user_vote", "my_user_vote");

function my_user_vote()
{
    $ID = $_REQUEST['post_id'];
    $action = $_REQUEST['action'];
    $title = $_REQUEST['title'];
    $content = $_REQUEST['content'];
    $category = $_REQUEST['category'];
    if ($ID) {
        $my_post = array(
            'ID' => $ID,
            'post_title' => $title,
            'post_content' => $content,
        );

        $updated_post = wp_update_post($my_post);
        wp_set_post_categories( $ID, array( $category ) );

        if ($updated_post) {
            $result['type'] = "success";
            $result['message'] = 'Post #'. $ID .' has been updated successfully';
            echo json_encode($result);
        }

    }
    die();

}