<?php get_header();

$args = [
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => 6,
];

$posts = new WP_Query($args);

?>
    <div class="tabs-container">
        <div class="tabs-row controllers">
            <a href="javascript:void(0)" onclick="openCity(event, 'Reports');">
                <div class="tabs-third tablink tabs-bottombar tabs-hover-light-grey tabs-padding tabs-border-red">
                    Reports
                </div>
            </a>
            <a href="javascript:void(0)" onclick="openCity(event, 'Controls');">
                <div class="tabs-third tablink tabs-bottombar tabs-hover-light-grey tabs-padding">Controls</div>
            </a>
        </div>

        <div id="Reports" class="tab">
            <div class="table-view">
                <div class="tabs-container main">
                <?php if ($posts->have_posts()) : ?>
                    <?php while ($posts->have_posts()) : $posts->the_post(); ?>
                        <?php $post_id = get_the_ID() ?>
                        <?php $title = the_title('', '', false) ?>
                        <?php $content = strip_tags(get_the_content()) ?>
                        <?php $category = get_the_category($post_id); ?>
                        <?php $categories = get_categories(["hide_empty" => 0]); ?>
                            <div class="tabs-card-4">
                                <header class="tabs-container tabs-light-grey">
                                    <h3 id="report_title_<?php echo $post_id ?>"><?php echo $title ?></h3>
                                </header>
                                <div class="tabs-container">
                                    <p id="report_category_<?php echo $post_id ?>"><?php echo $category[0]->name ?></p>
                                    <hr>
                                    <p class="reports-content" id="report_content_<?php echo $post_id ?>"><?php echo $content ?></p>
                                </div>
                            </div>
                    <?php endwhile; ?>
                <?php endif; ?>
                </div>
            </div>
        </div>

        <div id="Controls" class="tab" style="display:none">
            <div class="table-edit">
                <table class="tabs-table-all">
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th class="tabs-center">Category</th>
                        <th class="tabs-right">Action</th>
                    </tr>
                    <?php if ($posts->have_posts()) : ?>
                        <?php while ($posts->have_posts()) : $posts->the_post(); ?>
                            <?php $post_id = get_the_ID() ?>
                            <?php $title = the_title('', '', false) ?>
                            <?php $content = strip_tags(get_the_content()) ?>
                            <?php $category = get_the_category($post_id); ?>
                            <?php $categories = get_categories(["hide_empty" => 0]); ?>
                            <tr data-id="<?php echo $post_id ?>">
                                <td>
                                    <span class="row-data"
                                          id="row_title_<?php echo $post_id ?>"><?php echo $title ?></span>
                                    <input class="row-data-input" type="text" name="row_title"
                                           value="<?php echo $title ?>"/>
                                </td>
                                <td>
                                    <span class="row-data"
                                          id="row_content_<?php echo $post_id ?>"><?php echo $content ?></span>
                                    <textarea class="row-data-input" name="row_content" ><?php echo $content ?></textarea>
                                </td>
                                <td class="tabs-center">
                                    <span class="row-data"
                                          id="category_item_<?php echo $post_id ?>"><?php echo $category[0]->name ?></span>
                                    <select class="row-data-input" name="category_item">
                                        <?php foreach ($categories as $cat) : ?>
                                            <option <?php echo $category[0]->term_id == $cat->term_id ? 'selected' : '' ?>
                                                    value="<?php echo $cat->term_id ?>"><?php echo $cat->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td class="tabs-right">
                                    <a href="javascript:" class="edit-action" data-id="<?php echo $post_id ?>">Edit</a>
                                    <a href="javascript:" class="update-action" data-id="<?php echo $post_id ?>"
                                       style="display: none">Update</a>
                                    <a href="javascript:" class="cancel-action" data-id="<?php echo $post_id ?>"
                                       style="display: none">Cancel</a>
                                </td>
                            </tr>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </table>
            </div>
        </div>
    </div>
<?php get_footer() ?>